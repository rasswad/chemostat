from os.path import dirname, realpath
import sys
dir_path = dirname(realpath(__file__))
sys.path += [dir_path, dirname(dir_path)]

import numpy as np
from matplotlib import pyplot as plt

from chemostat.growth import Monod, Haldane
from chemostat.model import ConstantYield
from chemostat.plot import PhasePortraitExtended, PhasePortrait, \
    trajectory, equilibria


def plot_model(model: ConstantYield):
    pp = PhasePortraitExtended(model).plot()

    pp.suptitle("Phase portrait of constant yield chemostat model "
                f"with a {type(model.growth).__name__} growth rate")
    pp.set_axis_labels("$s(t)$: nutrient concentration",
                       "$x(t)$: biomass concentration",
                       "$\\mu(s)$: growth rate")
    return pp

def plot_trajectory(model, sol):
    fig = plt.figure(figsize=(8, 10))
    gs = plt.GridSpec(2, 1, height_ratios=(1, 3))

    E = model.equilibria()[-1]

    pp = PhasePortrait(model, ax=fig.add_subplot(gs[1,0])).plot()
    trajectory(sol)
    equilibria(E, labels="$E$")

    ax = fig.add_subplot(gs[0,0])
    for i in range(len(model)):
        label = "$" + model[i] + "(t)$"
        ax.plot(sol.t, sol.x[i], label=label)
        ax.axhline(y=E[i], xmin=0, xmax=1, color="black", linestyle="--")
        ax.legend(loc='upper right')
    return pp, ax

if __name__ == "__main__":
    monod = Monod(1, 0.2)
    cst_monod = ConstantYield(monod, 0.8, 2, 1)
    plot_model(cst_monod)
    
    haldane = Haldane(1, 0.2, 0.6)
    cst_haldane = ConstantYield(haldane, 0.6, 1, 1)
    plot_model(cst_haldane)

    initial = np.array([2, 2])
    t_range = 0, 50
    sol = cst_monod.calculate_trajectory(initial, t_range, max_step=0.05)
    plot_trajectory(cst_monod, sol)

    plt.show()