from os.path import dirname, realpath
import sys
dir_path = dirname(realpath(__file__))
sys.path += [dir_path, dirname(dir_path)]

import numpy as np
from matplotlib import pyplot as plt

from chemostat.growth import Monod
from chemostat.model import Trajectory, ConstantYield
from chemostat.observer import KalmanFilter
from chemostat.observer.model import conservation_KF_se, biomodelEKF


def batch_kalman(monod: ConstantYield, t, x0, x0_hat,
                 measure_noise, process_noise, P0_11, q,
                 y_step=1, N=100, plots=True):
    I1 = np.eye(1)

    # simulation
    sol = monod.calculate_trajectory(x0, (t[0], t[-1]), t_eval=t)
    tk = t[::y_step]
    yk = sol.x[1, ::y_step]

    # model parameters
    d = monod.D
    b = np.array([[d * monod.Sin, 0]]).T
    C = np.array([[0, 1]])
    G = np.array([[-1/monod.Y, 1]]).T

    # KF parameters
    P0d = [P0_11, measure_noise]
    P0 = np.diag(P0d)**2
    Q = I1 * (process_noise**2)
    R = I1 * (measure_noise**2)

    # filter objects
    rkf = KalmanFilter(2, 1, np.eye(2), C, b=b, R=R, Q=Q, G=G)
    ekf = biomodelEKF(monod, C, R=R, Q=Q, G=G)
    ck0 = conservation_KF_se(monod, R=R, Q=Q, pre_smooth=False)
    ck1 = conservation_KF_se(monod, R=R, Q=Q, pre_smooth=True)

    kalman = [rkf, ekf, ck0, ck1]

    # result arrays
    r = np.zeros((4, N, len(tk), 2))
    mu_hat = np.zeros((4, len(tk)))
    for i in range(N):
        y = yk + np.random.normal(scale=measure_noise, size=len(yk))
        x0_hat[1] = y[0]
        sig0 = x0_hat[0] + x0_hat[1] / monod.Y

        # A(t) for reference KF
        mu = lambda t: monod.mu(sol(t)[0])
        rkf.A = lambda t: np.array([
            [-d, -mu(t) / monod.Y],
            [0, mu(t) - d],
        ])

        # run Kalman Filters
        for j, kf in enumerate(kalman):
            if j < 2:
                r[j, i] = kf(x0_hat, P0, tk, y).xhat[:, :]
            else:
                obs = kf(x0_hat, sig0, P0, tk, y, q=q)
                r[j, i] = obs.xhat
                # add first mu estimation
                if i == 0:
                    mu_hat[j] = obs.mu

        # add first mu estimation
        if i == 0:
            mu_hat[0] = monod.mu(sol.x[0, ::y_step])
            mu_hat[1] = monod.mu(r[1, 0, :, 0])

    if plots:
        print("plotting results")
        f = lambda a: round(a, 5)
        t1 = f"$R={f(measure_noise)}^2,\\quad" \
             f"Q={f(process_noise)}^2$"
        t3 = f"$P0=diag({f(P0d[0])},{f(P0d[1])})^2$"
        t2 = "$,\\quad$" if len(t1) + len(t3) < 60 else "\n"
        plot_batch(tk, r, x=sol, mu=mu_hat, title=(t1 + t2 + t3))
    return r


def plot_batch(t, xhat: np.ndarray, x: Trajectory=None, mu=None, y=None, title=''):
    fig, axs = plt.subplots(2, 2, sharex=True, sharey=True, figsize=(15, 10))
    # fig title
    fig.subplots_adjust(top=0.8, left=0.05, right=0.85, wspace=0)
    t0 = f"Kalman Filter comparison (N={len(xhat[0])})"
    title = t0 + '\n' + title if title else t0
    fig.suptitle(title, y=0.95)
    for i in range(2):
        axs[i,1].tick_params(axis='y', which='both', left=False)
        axs[1,i].set_xlim(t[0], t[-1])

    N = xhat.shape[1]
    for j, ax in enumerate(axs.flatten()):
        if y is not None:
            ax.plot(t, y, ':', label=f"$y$", color='red')

        avg = np.mean(xhat[i], axis=0)
        std2 = 2 * np.std(xhat[i], axis=0)

        for i, v in enumerate(['s', 'e']):
            c = ax.plot(t, xhat[j,0,:,i], '--', label=f"$\\widehat{{{v}}}$")[0].get_color()
            if N > 1:
                ax.plot(t, avg[:,i], color=c, label=f"$\\bar{{{v}}}$")
                ax.fill_between(t, avg[:,i] + std2[:,i], avg[:,i] - std2[:,i],
                                color=c, alpha=0.5,
                                label=f"$\\bar{{{v}}}\pm 2\\sigma_{v}$")
            if x is not None:
                ax.plot(x.t, x.x[i], ':', color=c, label=f"${v}$")
        if mu is not None:
            ax.plot(t, mu[j], label=r"$\widehat{\mu}$")
    axs[1,1].legend(loc='lower right', bbox_to_anchor=(1.35, 0))
    axs[0,1].set_ylim(ymin=-0.1, ymax=2.1)

    axs[0,0].set_title(r"$\widehat{\mu}(t) = \mu(s(t))$")
    axs[0,1].set_title("Extended Kalman Filter")
    mu_hat = lambda y_hat: (r"$\widehat{\mu}(t) = \mu(\widehat{\sigma}(t) -"
                            + y_hat + r"/\gamma)$")
    axs[1,0].set_title(mu_hat(r"y_{k-1}"))
    axs[1,1].set_title(mu_hat(r"\widehat{y}_{k|k-1}(t))") + f"$, q = {q}$")
    return axs


if __name__ == "__main__":
    plt.rcParams.update({'font.size': 18})

    # model parameters
    gamma = 1
    mu_max = np.log(2)
    ks = 0.1
    d = 0.008 * 60
    s_in = 2
    monod = Monod(mu_max, ks)
    cst_monod = ConstantYield(monod, d, s_in, gamma)

    # initial conditions
    t = np.arange(0, 3000, 10) / 60
    x0 = np.array([0, 1])
    x0_hat = np.array([5, np.nan])
    
    # noise std / covariance (must be iterables)
    measure_noise = 0.05
    process_noise = 0.025
    P0_11 = 5
    q = 0.3

    batch_kalman(cst_monod, t, x0, x0_hat, measure_noise, process_noise, P0_11,
                 q, y_step=1, N=10)

    plt.show()
