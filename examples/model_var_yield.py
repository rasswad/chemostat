from os.path import dirname, realpath
import sys
dir_path = dirname(realpath(__file__))
sys.path += [dir_path, dirname(dir_path)]

import numpy as np
from matplotlib import pyplot as plt

from chemostat.growth import Monod, ShiftedMonod
from chemostat.model import VariableYield, ReducedModelTI
from chemostat.plot import PhasePortraitExtended, PhasePortrait, \
    trajectory, equilibria


def plot_model(model):
    pp = PhasePortraitExtended(model).plot()

    pp.suptitle("Phase portrait of the asymptotic variable yield model")
    pp.set_axis_labels("$s(t)$: nutrient concentration",
                       "$x(t)$: biomass concentration",
                       "$\\mu(s)$: growth rate")
    return pp

def plot_trajectory(model: ReducedModelTI, sol):
    fig = plt.figure(figsize=(8, 10))
    gs = plt.GridSpec(2, 1, height_ratios=(1, 3))

    E = model.equilibria()[-1]

    pp = PhasePortrait(model, ax=fig.add_subplot(gs[1,0])).plot()
    trajectory(sol)
    equilibria(E, labels="$E$")

    ax = fig.add_subplot(gs[0,0])
    model = model.base
    E = model.equilibria()[-1]
    for i in range(len(model)):
        label = "$" + model[i] + "(t)$"
        ax.plot(sol.t, sol.x[i], label=label)
        ax.axhline(y=E[i], xmin=0, xmax=1, color="black", linestyle="--")
        ax.legend(loc='upper right')
    return pp, ax


if __name__ == "__main__":
    rho = Monod(1, 0.2)
    mu = ShiftedMonod(1, 0.5, 0.5)
    droop = VariableYield(rho, mu, 0.6, 2, quota='total')
    droop2 = droop.reduce_model('z')
    kw = dict(stream_kw=dict(mesh=500, density=(0.5, 1)),
              quiver_kw=dict(mesh=20), D=droop.D, Sin=droop.Sin)
    pp1 = plot_model(droop2).plot(**kw)

    initial_2d = np.array([2, 2])
    initial = np.insert(initial_2d, 2, droop2.drop_func(initial_2d))
    t_range = 0, 50
    sol = droop.calculate_trajectory(initial, t_range, max_step=0.05)
    pp2, ax = plot_trajectory(droop2, sol)
    kw.pop('D')
    kw.pop('Sin')
    pp2.plot(**kw)

    plt.show()