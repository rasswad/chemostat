from os.path import dirname, realpath
import sys
dir_path = dirname(realpath(__file__))
sys.path += [dir_path, dirname(dir_path)]

import numpy as np
import matplotlib as mpl
from matplotlib import colormaps as cm
from matplotlib import pyplot as plt

from chemostat.growth import Monod, ShiftedMonod
from chemostat.model import ABModel


def x_eq(ab: ABModel, d, s_in=None, beta=None):
    if s_in is None:
        s_in = ab.Sin
    if beta is None:
        beta = ab.beta
    s = ab.mu_e.inverse(d)
    e = ab.Y * np.maximum(0, s_in - s)
    v = ab.phi.inverse(d)
    v_in = beta * e
    z = np.maximum(0, v_in - v)
    q = ab.mu_c.inverse(d)
    c = z / q
    nan = np.isnan(c)
    c[nan] = 0
    return np.array([s, e, v, c, z])


def op_diag_view(ab: ABModel):
    s_in = np.arange(0, 5, 0.05)
    d = np.arange(0, 1, 0.01)
    S_in, D = np.meshgrid(s_in, d)
    X = x_eq(ab, D, S_in)

    fig = plt.figure(figsize=(12, 6))
    ax = np.empty(3, dtype=mpl.axes._axes.Axes)
    gs = fig.add_gridspec(1, 3, width_ratios=(1, 1, 0.05))
    ax[0] = fig.add_subplot(gs[0])
    ax[1] = fig.add_subplot(gs[1], sharex=ax[0], sharey=ax[0])
    ax[2] = fig.add_subplot(gs[2])
    ax[1].tick_params(axis='y', which='both', labelleft=False)
    cm_kw = dict(cmap=cm.get_cmap('inferno'), shading='gouraud',
                 vmin=0, vmax=np.max(X[[False, True, False, True, False]]))
    m0 = ax[0].pcolormesh(S_in, D, X[1], **cm_kw)
    m1 = ax[1].pcolormesh(S_in, D, X[3], **cm_kw)
    cb = fig.colorbar(m1, cax=ax[2], label=r"Biomass concentration $[gL^{-1}]$")
    ax[0].set_title("Bacteria biomass equilibrium $e^*$")
    ax[1].set_title("Algae biomass equilibrium $c^*$")
    ax[0].set_xlabel(r"Glucose feed $s_{in}~[gL^{-1}]$")
    ax[1].set_xlabel(r"Glucose feed $s_{in}~[gL^{-1}]$")
    ax[0].set_ylabel("Dilution rate $d~[h^{-1}]$")
    fig.suptitle(r"Operational diagram for fixed vitamin production $\beta="
                 f"{nr(ab.beta,1)}$")

    xlim = ax[0].get_xlim()
    ax[0].plot(s_in, ab.mu_e(s_in), color='red')
    ax[1].plot(s_in, ab.mu_e(s_in), '--', color='red')
    ax[0].plot(ab.psi.inverse(d), d, '--', color='red')
    ax[1].plot(ab.psi.inverse(d), d, color='red')
    ax[0].set_xlim(xlim)
    gs.tight_layout(fig)
    gs.update(wspace=0.05)

    if save:
        plt.savefig(f"{SAVE_DIR}/op_diag.pdf")
    return fig, ax


def biomass_fixed_beta(ab: ABModel):
    d = np.arange(0, 0.7, 0.01)
    s_in = np.arange(0, 5.1, 0.1)
    d_step = 10
    s_step = 5
    S_in, D = np.meshgrid(s_in, d)
    X = x_eq(ab, D, S_in)[[False, True, False, True, False]]

    fig, ax = plt.subplots(figsize=(8, 8))
    ax.plot(X[0], X[1], color='grey', marker='.', ls='None', alpha=0.2)

    # d-isoclines
    for i in range(0, len(d), d_step):
        ax.plot(X[0, i], X[1, i], color='C0')
        ax.text(X[0, i, -1]+0.1, X[1, i, -1], f"{nr(d[i], 2)}", color='C0')
    # s_in-isoclines
    for j in range(s_step, len(s_in), s_step):
        ax.plot(X[0, :, j], X[1, :, j], color='C3')
        ax.text(X[0, 0, j]-0.2, X[1, 0, j]+0.25, f"{nr(s_in[j], 2)}", color='C3')

    ax.set_title(r"Biomass equilibria sensitivity to $(d,s_{in})$"
                 f" with $\\beta={nr(ab.beta,1)}$")
    ax.set_xlabel(r"Bacteria biomass $e^*~[gL^{-1}]$")
    ax.set_ylabel(r"Algae biomass $c^*~[gL^{-1}]$")
    ax.plot([], [], color='C0', label=r'$d$')
    ax.plot([], [], color='C3', label=r'$s_{in}$')
    ax.legend(loc='upper left')
    ax.set_xlim(0, 5.5)
    ax.set_ylim(0, 11)
    if save:
        plt.savefig(f"{SAVE_DIR}/sensitivity_fixed_beta.pdf")
    return ax


def biomass_fixed_Sin(ab: ABModel):
    d = np.arange(0, 0.7, 0.01)
    beta = np.arange(0, 2.01, 0.05)
    d_step = 10
    b_step = 5
    B, D = np.meshgrid(beta, d)
    X = x_eq(ab, D, beta=B)[[False, True, False, True, False]]

    fig, ax = plt.subplots(figsize=(8, 8))
    ax.plot(X[0], X[1], color='grey', marker='.', ls='None', alpha=0.2)

    # d-isoclines
    for i in range(0, len(d), d_step):
        ax.plot(X[0, i], X[1, i], color='C0')
        ax.text(X[0, i, -1]-0.02, X[1, i, -1]+0.2, f"{nr(d[i], 2)}", color='C0')
    # beta-isoclines
    for j in range(b_step, len(beta), b_step):
        ax.plot(X[0, :, j], X[1, :, j], color='C2')
        ax.text(X[0, 0, j]+0.01, X[1, 0, j], f"{nr(beta[j], 2)}", color='C2')

    ax.set_title(r"Biomass equilibria sensitivity to $(d,\beta)$"
                 f" with $s_{{in}}={nr(ab.Sin,1)}$")
    ax.set_xlabel(r"Bacteria biomass $e^*~[gL^{-1}]$")
    ax.set_ylabel(r"Algae biomass $c^*~[gL^{-1}]$")
    ax.plot([], [], color='C0', label=r'$d$')
    ax.plot([], [], color='C2', label=r'$\beta$')
    ax.legend(loc='upper left')
    ax.set_xlim(2.1, 2.55)
    ax.set_ylim(0, 11)
    if save:
        plt.savefig(f"{SAVE_DIR}/sensitivity_fixed_feed.pdf")
    return ax


def biomass_region_fixed_Sin(ab: ABModel):
    s_in = np.arange(1, 4, 0.25)
    d = np.arange(0, 0.7, 0.01)
    beta = np.arange(0, 2.01, 0.5)
    S_in, B, D = np.meshgrid(s_in, beta, d)
    X = x_eq(ab, D, s_in=S_in, beta=B)[[False, True, False, True, False]]
    Xminor = X[:, :, :, ::10] # biomass, beta, s_in, d

    fig, ax = plt.subplots(figsize=(8, 8))
    c = [f'C{i}' for i in range(len(s_in))]
    for i in range(len(s_in)):
        ax.fill_between(X[0,0,i].flatten(), X[1,-1,i].flatten(),
                        X[1,0,i].flatten(), alpha=0.4, color=c[i],
                        label = f"$s_{{in}}={nr(s_in[i], 1)}$")
        #ax.scatter(Xminor[0, :, i], Xminor[1, :, i], marker='.', color=c[i])

    ax.set_title("Identification of the parameter $s_{in}$ bounds\n"
                 "for reaching objective biomass equilibria $(e^*, c^*)$")
    ax.set_xlabel(r"Bacteria biomass $e^*~[gL^{-1}]$")
    ax.set_ylabel(r"Algae biomass $c^*~[gL^{-1}]$")
    ax.legend(loc='upper left', ncols=2)
    ax.set_ylim(0, 16)
    if save:
        plt.savefig(f"{SAVE_DIR}/s_in_bounds.pdf")
    return ax


if __name__ == "__main__":
    ab = ABModel(Monod(1, 0.2),
                 Monod(1, 0.2),
                 ShiftedMonod(1, 0.5, 0.5),
                 0.8, 2.5, 1, 1, quota='total')

    mpl.rcParams.update({
        "font.size": 14,
        "font.family": "serif",
        "mathtext.fontset": "cm",
        "savefig.dpi": 300,
    })
    nr = lambda a, n: round(a, n)
    save = False
    SAVE_DIR = "/home/rand/repo/chemostat/plots/2023_11_23"

    # operational diagram view
    op_diag_view(ab)

    # input sensitivity view
    biomass_region_fixed_Sin(ab)
    biomass_fixed_beta(ab)
    biomass_fixed_Sin(ab)

    plt.show()
