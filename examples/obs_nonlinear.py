from os.path import dirname, realpath
import sys
dir_path = dirname(realpath(__file__))
sys.path += [dir_path, dirname(dir_path)]

import numpy as np
from matplotlib import pyplot as plt

try:
    import chemostat as cs
except:
    from os import chdir
    chdir('..')
    import chemostat as cs

from chemostat.growth import Monod
from chemostat.model import Trajectory, ConstantYield
from chemostat.observer import LuenbergerLike
from chemostat.observer.model import BiomodelEKF, cst_yield_hg
from chemostat.observer.model import biomodelEKF, cst_yield_hg


def batch_nonlinear(monod: ConstantYield, t, x0, x0_hat,
                    measure_noise, L, Q, P0_11, theta,
                    y_step=1, N=100, plots=True):

    # simulation
    sol = monod.calculate_trajectory(x0, (t[0], t[-1]), t_eval=t)
    tk = t[::y_step]
    yk = sol.x[1, ::y_step]

    # observers
    C = np.array([[0, 1]])
    G = np.array([[-1/monod.Y, 1]]).T
    P0d = [P0_11, measure_noise]
    P0 = np.diag(P0d)**2
    lu = LuenbergerLike(monod, measure_matrix=C)
    ekf = biomodelEKF(monod, C, G=G)
    ekf.Q = np.eye(1) * (Q**2)
    hg = cst_yield_hg(monod, scale=1.0)

    # result array
    r = np.zeros((3, N, len(tk), 2)) # observers x iterations x time x dim
    for i in range(N):
        y = yk + np.random.normal(scale=measure_noise, size=len(yk))
        x0_hat[1] = y[0]

        r[0, i, :, :] = lu(x0_hat, tk, y, L).xhat[:, :]
        r[1, i, :, :] = ekf(x0_hat, P0, tk, y).xhat[:, :]
        r[2, i, :, :] = hg(x0_hat, tk, y, theta, first_step=1e-2).xhat[:, :]

    if plots:
        params = dict(R=measure_noise, L=L, Q=Q, P0=P0d, theta=theta)
        plot_batch(tk, r, sol, y, params)
    return r


def plot_batch(t, xhat: np.ndarray, x: Trajectory=None, y=None, params={}):
    fig, axs = plt.subplots(3, sharex=True, sharey=True, figsize=(15, 10))
    fig.subplots_adjust(left=0.05, right=0.85)
    N = xhat.shape[1]

    for i, ax in enumerate(axs):
        if y is not None:
            ax.plot(t, y, ':', label=f"$y$", color='red')

        avg = np.mean(xhat[i], axis=0)
        std2 = 2 * np.std(xhat[i], axis=0)

        for j, v in enumerate(['s', 'e']):
            c = ax.plot(t, xhat[i,0,:,j], '--',
                        label=f"$\\widehat{{{v}}}$")[0].get_color()
            if N > 1:
                ax.plot(t, avg[:,j], color=c, label=f"$\\bar{{{v}}}$")
                ax.fill_between(t, avg[:,j] + std2[:,j], avg[:,j] - std2[:,j],
                                color=c, alpha=0.5,
                                label=f"$\\bar{{{v}}} \pm 2\\sigma_{v}$")
            if x is not None:
                ax.plot(x.t, x.x[j], ':', color=c, label=f"${v}$")

    axs[-1].legend(loc='lower right', bbox_to_anchor=(1.15, 0))
    axs[-1].set_xlim(t[0], t[-1])
    axs[-1].set_ylim(-0.1, 3.5)

    # titles
    f = lambda a: round(a, 5)
    title = "Nonlinear observer comparison"
    if N > 1: title += f" (N={N})"
    title += f" with measurement noise std {f(params['R'])}"
    fig.suptitle(title)
    L = np.squeeze(params['L'])
    axs[0].set_title(f"Luenberger Observer with $L=({f(L[0])}, {f(L[1])})^T$")
    P = params['P0']
    axs[1].set_title(f"Extended Kalman Filter with $Q={f(params['Q'])}^2,"
                    f"P0=diag({f(P[0])},{f(P[1])})^2$")
    axs[2].set_title(f"High Gain Observer with $\\theta={f(params['theta'])}$")
    return axs


if __name__ == "__main__":
    plt.rcParams.update({'font.size': 18})

    # model parameters
    gamma = 1
    mu_max = np.log(2)
    ks = 0.1
    d = 0.008 * 60
    s_in = 2
    monod = Monod(mu_max, ks)
    cst_monod = ConstantYield(monod, d, s_in, gamma)

    # initial conditions
    t = np.arange(0, 2000, 10) / 60
    x0 = np.array([0.5, 1])
    x0_hat = np.array([10, np.nan])

    noise_std = 0.25

    # observer parameters
    L = np.array([[1, 1]]).T * 0.01
    Q_sqrt = 0.005
    P0_11 = 10
    theta = 0.001

    batch_nonlinear(cst_monod, t, x0, x0_hat, noise_std, L, Q_sqrt, P0_11, theta,
                    y_step=1, N=10, plots=True, saveplot=False)
    plt.show()
