from abc import ABC, abstractmethod

import numpy as np
from scipy.integrate import solve_ivp

from chemostat import utils
from chemostat.model import Biomodel

class StateEstimation(utils.OptimizeResult):
    pass

class Observer(ABC):
    @abstractmethod
    def measure_fun(self, *args) -> np.ndarray:
        raise NotImplementedError

    @abstractmethod
    def dynamics_fun(self, *args) -> np.ndarray:
        raise NotImplementedError

    @abstractmethod
    def _get_ode(self, *args):
        raise NotImplementedError

    def __call__(self, x0, t, y, *args, **kwargs):
        t_span = t[0], t[-1]
        obs = solve_ivp(self._get_ode(), t_span, x0, t_eval=t,
                        vectorized=False, **kwargs)
        if obs.success:
            self.estimate = StateEstimation(t=t, xhat=obs.y.T, y=y)
        else:
            raise RuntimeError(obs.message)
        return self.estimate