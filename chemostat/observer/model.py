import numpy as np

from ._luenberger import LuenbergerLike
from ._kalman import KalmanFilter, ExtendedKalmanFilter
from ._high_gain import Diffeomorphism, HighGainSISO
from ._compound import ConservationKF, BayesianKF
from chemostat.model import Biomodel, ConstantYield, FluoCstYield


def conservation_KF_se(model: ConstantYield, R=None, Q=None, pre_smooth=True):
    mu = model.mu
    gamma = model.Y
    d = model.D
    s_in = model.Sin
    mu_x = lambda sig, y: mu(sig - (y/gamma))
    A_func = lambda mu_hat: np.array([
        [-d, -mu_hat / gamma],
        [0, mu_hat - d],
    ])
    sigma = lambda t, t0, sig0: s_in + (sig0 - s_in) * np.exp(d*(t0 - t))
    b = np.array([[d * s_in, 0]]).T
    G = np.array([[-1/gamma, 1]]).T
    return ConservationKF(model, mu_x, A_func, sigma, b=b, R=R, Q=Q, G=G,
                          pre_smooth=pre_smooth)

def conservation_KF_sef(model: FluoCstYield, R=None, Q=None, fluo_sigma='post'):
    mu = model.mu
    gamma = model.Y
    d = model.D
    s_in = model.Sin
    a = model.a
    mu_x = lambda sig, y: mu(sig - (y/(a*gamma)))
    A_func = lambda mu_hat: np.array([
        [-d, -mu_hat / gamma, 0],
        [0, (1-a)*mu_hat - d, 0],
        [0,  a*mu_hat, -d],
    ])
    sigma = lambda t, t0, sig0: s_in + (sig0 - s_in) * np.exp(d*(t0 - t))
    b = np.array([[d * s_in, 0, 0]]).T
    G = np.array([[-1/gamma, 1 - a, a]]).T
    return ConservationKF(model, mu_x, A_func, sigma, b=b, R=R, Q=Q, G=G,
                          fluo_sigma=fluo_sigma)

def bayesian_KF_sef(model: FluoCstYield, R=None):
    x_in = np.array([model.Sin, 0, 0])
    C = np.array([[0, 0, 1, 0]])
    return BayesianKF(model.N, model.D, x_in, C, R=R)

def biomodelEKF(model: Biomodel, h, Jh=None, R=None, Q=None, G=None):
    f = model._get_ode(1)
    Jf = model.jacobian
    if Jh is None:
        if isinstance(h, np.ndarray) and h.ndim == 2:
            Jh = h
            h = lambda x: np.dot(Jh, x)
        else:
            raise ValueError("'Jh' cannot be 'None' if 'h' is not a matrix")
    return ExtendedKalmanFilter(len(model), Jh.shape[0], f, h, Jf, Jh, R, Q, G)


def cst_yield_hg(model: ConstantYield, scale=1.0):
    mu = model.mu
    phi = lambda x: np.array([x[1], scale * mu(x[0]) * x[1]])
    phi_inv = lambda z: np.array([mu.inverse(z[1]/(scale * z[0])), z[0]])
    phi_jac = lambda x: np.array([
        [0, 1],
        [scale * mu.derivative(x[0]) * x[1], scale * mu(x[0])],
    ])
    diffeo = Diffeomorphism(len(model), phi, phi_inv, phi_jac)
    hg = HighGainSISO(model, diffeo)
    hg.S1 = np.array([[1, -1], [-1, 2]])
    return hg
