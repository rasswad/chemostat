import numpy as np

from ._luenberger import LuenbergerLike, Biomodel


class Diffeomorphism(object):
    def __init__(self, dim, func, inv, jacobian):
        self.dim = dim
        self.__func = func
        self.__inv = inv
        self.__jac = jacobian
    def __call__(self, x): return self.__func(x)
    def inverse(self, z): return self.__inv(z)
    def jacobian(self, x): return self.__jac(x)


S_matrix = lambda theta, n: np.power(
    float(theta),
    [[-(i + j + 1) for j in range(n)] for i in range(n)]
)

class HighGainSISO(LuenbergerLike):
    def __init__(self, dynamic_fun, diffeo: Diffeomorphism):
        self.dim = diffeo.dim
        C = np.zeros((1, self.dim))
        C[0,0] = 1
        LuenbergerLike.__init__(self, dynamic_fun, measure_matrix=C)
        fx = self.f
        def fz(t, z):
            x = diffeo.inverse(z)
            return np.dot(diffeo.jacobian(x), fx(t, x))
        self.f = fz
        self.diffeo = diffeo
        self.S1 = np.ones((self.dim, self.dim))
        self.gain = np.ones((self.dim, self.dim))

    def calculate_gain(self, theta, S1=1):
        self.S = S1 * S_matrix(theta, self.dim)
        self.gain = np.linalg.inv(self.S) @ self.C.T
        return self.gain

    def __call__(self, x0, t, y, theta, S1=None, **kwargs):
        if S1 is None: S1 = self.S1
        self.calculate_gain(theta, S1)
        z0 = self.diffeo(x0)
        self.estimate = LuenbergerLike.__call__(self, z0, t, y, self.gain, **kwargs)
        self.estimate.zhat = self.estimate.xhat
        self.estimate.xhat = self.diffeo.inverse(self.estimate.zhat.T).T
        return self.estimate
