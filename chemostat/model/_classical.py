import numpy as np

from chemostat.growth import *
from chemostat.utils import nonnegativeode
from ._base import BiomodelTI, ReducedModelTI


class ConstantYield(BiomodelTI):
    def __init__(self, growth: GrowthRate, dilution_rate, nutrient_feed, yield_):
        super().__init__()
        self._abbrv = dict(
            mu='growth',
            D='dilution_rate',
            Sin='nutrient_feed',
            Y='yield_',
        )
        self._dim = 2
        self.set_vars('s', 'x')
        self.mu = growth
        self.D = dilution_rate
        self.Sin = nutrient_feed
        self.Y = yield_

    # @nonnegativeode
    def ode(self, state):
        dot = super().ode(state)
        if isinstance(self.mu, GrowthRateSimple):
            mu = self.mu(state[0])
        elif isinstance(self.mu, GrowthRateSX):
            mu = self.mu(state[0], state[1])
        else:
            raise ValueError("unknown or incompatible GrowthRate")
        mu_x = mu * state[1]
        dot[0] = self.D * (self.Sin - state[0]) - (mu_x / self.Y)
        dot[1] = mu_x - self.D * state[1]
        return dot

    def equilibria(self):
        if not isinstance(self.mu, GrowthRateSimple):
            ValueError("only defined for GrowthRateSimple")
        E = [[self.Sin, 0]]
        xs = lambda s: self.Y * (self.Sin - s)
        if hasattr(self.mu, 'inverse'):
            S = [self.mu.inverse(self.D)]
        elif hasattr(self.mu, 'preimages'):
            S = self.mu.preimages(self.D)
        else:
            raise ValueError("GrowthRate is too complicated to find equilibria")
        for s in S:
            if s < self.Sin:
                E.append([s, xs(s)])
        return np.array(E)

    def jacobian(self, state):
        #self._checkdim(state)
        assert isinstance(self.mu, GrowthRateSimple), "can only calculate jacobian for GrowthRateSimple"
        s, x = state[0], state[1]
        dmu_x = self.mu.derivative(s) * x
        mu = self.mu(s)
        J = np.array([
            [-dmu_x / self.Y - self.D, -mu / self.Y],
            [dmu_x, mu - self.D],
        ])
        return J


class VariableYield(BiomodelTI):
    def __init__(self, nutrient_uptake: Monod, growth_velocity: ShiftedMonod,
                 dilution_rate, nutrient_feed, quota='total'):
        super().__init__()
        self._abbrv = dict(
            rho='nutrient_uptake',
            mu='growth_velocity',
            D='dilution_rate',
            Sin='nutrient_feed',
            phi='growth',
        )
        self._dim = 3
        self.quota = quota
        self.rho = nutrient_uptake
        self.mu = growth_velocity
        self.phi = VarYieldMonod(self.rho, self.mu)
        self.D = dilution_rate
        self.Sin = nutrient_feed

    @property
    def quota(self):
        return self.__quota

    @quota.setter
    def quota(self, value: str):
        value = value.lower()
        if value not in ['total', 'cell']:
            raise ValueError("'quota' has to be 'total' (default) or 'cell'")
        self.__quota = value
        var = 'q' if value == 'cell' else 'z'
        new_vars = self._vars[:2] + [var] if self._vars else ['s', 'x', var]
        self.set_vars(new_vars)

    @nonnegativeode
    def ode(self, state):
        dot = super().ode(state)
        rho = self.rho(state[0])
        with np.errstate(divide='ignore', invalid='ignore'):
            mu = self.mu(state[2]) if self.quota == 'cell' else self.mu(state[2] / state[1])
        rho_x = rho * state[1]
        dot[0] = self.D * (self.Sin - state[0]) - rho_x
        dot[1] = (mu - self.D) * state[1]
        dot[2] = rho_x - self.D * state[2] if self.quota == 'total' else rho_x - mu * state[2]
        return dot

    def equilibria(self):
        s1 = self.phi.inverse(self.D)
        if self.quota == 'total':
            E = [[self.Sin, 0, 0]]
            if s1 < self.Sin:
                z1 = self.Sin - s1
                E.append([s1, z1 / self.mu.inverse(self.D), z1])
        else:
            E = [[self.Sin, 0, self.mu.inverse(self.phi(self.Sin))]]
            if s1 < self.Sin:
                q1 = self.mu.inverse(self.D)
                E.append([s1, (self.Sin - s1) / q1, q1])
        return np.array(E)

    def jacobian(self, state):
        self._checkdim(state)
        assert isinstance(self.mu, GrowthRateSimple), "can only calculate jacobian for GrowthRateSimple"
        s, x = state[0], state[1]
        rho = self.rho(s)
        drho = self.rho.derivative(s)
        if self.quota == 'cell':
            q = state[2]
            mu = self.mu(q)
            dmu = self.mu.derivative(q)
            J = np.array([
                [-drho * x - self.D, -rho, 0],
                [0, mu - self.D, dmu * x],
                [drho, 0, -mu - dmu * q],
            ])
        else:
            z = state[2]
            q = 0 if x == 0 else z / x
            mu = self.mu(q)
            dmu = self.mu.derivative(q)
            J = np.array([
                [-drho * x - self.D, -rho, 0],
                [0, dmu * q + mu - self.D, dmu],
                [drho, rho, -self.D],
            ])
        return J

    def reduce_model(self, drop_dim):
        if drop_dim in [0, self._vars[0]]:
            if self.quota == 'cell':
                func = lambda u: self.Sin - u[1] * u[2]
            else:
                func = lambda u: self.Sin - u[2]
        elif drop_dim in [1, self._vars[1]]:
            if self.quota == 'cell':
                def func(u):
                    with np.errstate(divide='ignore', invalid='ignore'):
                        return (self.Sin - u[0]) / u[2]
            else:
                raise ValueError(f'cannot drop biomass _dimension in total quota system, ',
                                 f'use cell quota system instead or drop a different _dimension')
        elif drop_dim in [2, self._vars[2]]:
            if self.quota == 'cell':
                def func(u):
                    with np.errstate(divide='ignore', invalid='ignore'):
                        return (self.Sin - u[0]) / u[1]
            else:
                func = lambda u: self.Sin - u[0]
        else:
            ValueError('cannot drop {drop__dim} _dimension from this system')
        return ReducedModelTI(self, drop_dim, func)

    def reconstruct_missing(self, state, missing_dim):
        raise NotImplementedError
