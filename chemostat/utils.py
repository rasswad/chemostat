import numpy as np
from scipy.optimize._optimize import _dict_formatter, OptimizeResult
from scipy.interpolate import interp1d
from scipy.integrate._ivp.ivp import OdeResult, solve_ivp as sp_ivp


# wrapper functions
def sample_and_hold(t, y, axis=0):
    return interp1d(t, y, kind='previous', fill_value='extrapolate', axis=axis)

def solve_ivp(fun, t_span, y0, method='RK45', t_eval=None, dense_output=False,
              events=None, vectorized=False, args=None, t_first=True, **options):
    ode = sp_ivp(fun, t_span, y0, method, t_eval, dense_output, events,
                 vectorized, args, **options)
    assert isinstance(ode, OdeResult)
    if t_first:
        x = ode.y.T
        sol = lambda t: ode.sol(t).T if dense_output else None
    else:
        x = ode.y
        sol = ode.sol
    return OdeResult(t=ode.t, x=x, sol=sol, t_events=ode.t_events,
                     y_events=ode.y_events, nfev=ode.nfev, njev=ode.njev,
                     nlu=ode.nlu, status=ode.status, message=ode.message,
                     success=ode.success)

solve_ivp.__doc__ = sp_ivp.__doc__

# general functions

def iszero(x, precision=1E-15, p_norm=np.inf):
    """checks if a number, vector, or matrix is zero"""
    return np.linalg.norm(x, p_norm) < precision

def eval_func(x=None, y=None, func: callable=None, n=100):
    """Evaluates function of one variable over an interval
    two variables out of (x, y, func) must be given
    """
    def tuple_to_linspace(lim, n):
        assert len(lim) == 2, ValueError(f"first argument must be a tuple of 2")
        return np.linspace(lim[0], lim[1], n)
    if x is not None:
        if isinstance(x, tuple):
            x = tuple_to_linspace(x, n)
        else:
            x = np.array(x)
        if y is None:
            if func is None:
                raise ValueError("both 'y' and 'func' are 'None'")
            y = func(x)
    else:
        if isinstance(y, tuple):
            y = tuple_to_linspace(y, n)
        elif y is None:
            raise ValueError("both 'x' and 'y' are 'None'")
        else:
            y = np.array(y)
        if func is None:
            raise ValueError("both 'x' and 'func' are 'None'")
        x = func(y)
    return x, y

# decorators

def nonnegativeode(ode_func):
    def nn_ode(model, state):
        model._checkdim(state)
        dot = ode_func(model, state)
        dot[state < 0] = np.nan
        return dot
    return nn_ode

def reduce_dim(func):
    def reduced_func(model, state):
        model._checkdim(state)
        value = model.drop_func(state)
        full_state = np.insert(state, model.drop_dim, value, axis=0)
        base_func = model.base.__getattribute__(func.__name__)
        full = base_func(full_state)
        out = np.delete(full, model.drop_dim, axis=0)
        if func.__name__=='ode':
            return out
        if func.__name__=='jacobian':
            return np.delete(out, model.drop_dim, axis=1)
        raise NotImplementedError
    return reduced_func

# growth function helpers

def get_last_finite(f, a=0, b=1, tol=1E-16):
    x, dx = np.linspace(a, b, 100, retstep=True)
    y = f(x)
    finite = np.isfinite(y)
    last = np.where(finite)[0][-1]
    if dx <= tol:
        return x[last]
    a, b = x[last] - dx, x[last] + dx
    return get_last_finite(f, a, b, tol)

# old function helpers

def __get_dmax(mu, dstep=0.1, maxiter=10000):
    assert dstep > 0, "'dstep' must be positive"
    d0, d1 = 0, dstep
    s0, s1 = mu.inverse(d0), mu.inverse(d1)
    it = 0
    while s1 >= s0 and s1 > 0 and it < maxiter:
        d0, s0 = d1, s1
        d1 += dstep
        s1 = mu.inverse(d1)
        it += 1
    if it==maxiter:
        raise ValueError("'maxiter' reached")
    return d0

def __find_zero(func, a, b, tol):
    x = np.linspace(a, b, 10)
    ind = np.abs(func(x)).argmin()
    if 0.1 * (b-a) <= tol:
        return x[ind]
    return __find_zero(func, x[ind - 1], x[ind + 1], tol)

def preimage_of_monotonic(func, y, a, b, tol=1E-16):
    if a >= b:
        raise ValueError("[a,b] must be a valid interval")
    f_ab = func(np.linspace(a, b)) - y
    if np.all(f_ab[:-1] <= f_ab[1:]):
        # increasing
        f = lambda x: func(x) - y
    elif np.all(f_ab[:-1] >= f_ab[1:]):
        # decreasing
        f = lambda x: y - func(x)
    else:
        raise ValueError("'func' is not monotonic on the interval [a,b]")
    return __find_zero(f, a, b, tol)

# 2D model plot helpers

def call_ode_xy(x, y, model):
    """wrapper around 'model.ode' for 2D plots"""
    assert 2==len(model), "'model' is not 2-dimensional"
    X, Y = np.meshgrid(x, y)
    XY = np.array([X, Y])
    dXY = model.ode(XY)
    return dXY[0], dXY[1]

