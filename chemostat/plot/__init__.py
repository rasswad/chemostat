from ._growth import growthrate
from ._phaseportrait import *

__all__ = [
    "growthrate",
    "quiver", "streamplot", "trajectory", "equilibria", "eigenspaces",
    "phaseportrait", "extendedphaseportrait",
    "PhasePortrait", "PhasePortraitExtended",
]