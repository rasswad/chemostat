import warnings

import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt

from chemostat import utils
from chemostat.growth import *
from chemostat.model import *


def growthrate(x=None, y=None, growth: GrowthRate=None, ax=None,
               n_pts=500, **kwargs):
    try:
        if x and not y:
            x, y = utils.eval_func(x=x, y=y, func=growth, n=n_pts)
        elif y and not x:
            with warnings.catch_warnings():
                warnings.simplefilter('ignore')
                x, y = utils.eval_func(x=x, y=y, func=growth.inverse, n=n_pts)
    except NotImplementedError:
        raise ValueError("Cannot evaluate growth function from given data")
    if ax is None:
        ax = plt.gca()
    return ax.plot(x, y, **kwargs)

# TODO: operational diagram