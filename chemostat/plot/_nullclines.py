import warnings

import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt

from ..utils import eval_func
from ..model import *

__all__ = ["plot_nullclines"]

def _tex_dot(s: str): return "$\\dot{" + s + "}(t)=0$"

def plot_nullclines(model: BiomodelTI, var=[], ax=None):
    if isinstance(model, ConstantYield):
        return plot_nullclines_cst_yield(model, var, ax=ax)
    if isinstance(model, VariableYield):
        return plot_nullclines_var_yield(model, var, ax=ax)
    if isinstance(model, ReducedModelTI):
        if model.drop_dim == 2:
            return plot_nullclines_var_yield(model.base, var, ax=ax)
        warnings.warn("cannot plot isoclines for this reduced model.")
    return ax

def plot_nullclines_cst_yield(model: ConstantYield, var=[], xlim=None,
                             ylim=None, ax=None, n_pts=100):
    if ax is None:
        ax = plt.gca()
    else:
        if xlim is None: xlim = ax.get_xlim()
        if ylim is None: ylim = ax.get_ylim()
    
    lines = []

    if not var:
        var = list(range(len(model))) + ['sigma']
    
    if 0 in var or model[0] in var:
        ds_xs = lambda s: (model.Y*model.D)*(model.Sin - s) / model.growth(s)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            x, y = eval_func(x=xlim, func=ds_xs, n=n_pts)
            lines += ax.plot(x, y, label=_tex_dot(model[0]))
    
    if 1 in var or model[1] in var:
        c = ax._get_lines.get_next_color()
        lines.append(
            ax.axhline(y=0, xmin=0, xmax=1, label=_tex_dot(model[1]), color=c))
        if hasattr(model.growth, 'inverse'):
            S = [model.growth.inverse(model.D)]
        elif hasattr(model.growth, 'preimages'):
            S = model.growth.preimages(model.D)
        for s in S:
            if s != np.nan and s < model.Sin:
                lines.append(ax.axvline(x=s, ymin=0, ymax=1, color=c))
    
    if 'sigma' in var:
        x = np.array([0, model.Sin])
        y = model.Y*model.Sin - x
        lines += ax.plot(x, y, label=_tex_dot("\\sigma"))
    
    return lines

def plot_nullclines_var_yield(model: VariableYield, var=[], xlim=None,
                              ylim=None, ax=None, n_pts=100):
    if ax is None:
        ax = plt.gca()
    else:
        if xlim is None: xlim = ax.get_xlim()
        if ylim is None: ylim = ax.get_ylim()
    
    lines = []

    sx = model.reduce_model(2)
    x_sq = lambda s, q: (model.Sin - s) / q
    ds_xs = lambda s: model.D*(model.Sin - s) / model.rho(s)
    dq_sq = lambda q: model.rho.inverse(model.mu(q) * q)
    dq_xq = lambda q: x_sq(dq_sq(q), q)
    dq_qs = lambda s: model.mu.inverse(model.growth(s))
    dq_xs = lambda s: x_sq(s, dq_qs(s))
    dz_zs = lambda s: model.rho(s) / model.D

    if not var:
        var = list(range(len(model)))
    
    if 0 in var or model[0] in var: # ds = 0
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            s, x = eval_func(x=xlim, func=ds_xs, n=n_pts)
            lines += ax.plot(s, x, label=_tex_dot(model[0]))
    
    if 1 in var or model[1] in var: # dx = 0
        c = ax._get_lines.get_next_color()
        lines.append(
            ax.axhline(y=0, xmin=0, xmax=1, label=_tex_dot(model[1]), color=c))
        q = model.mu.inverse(model.D)
        if q != np.nan and q > 0:
            dx_xs = lambda s: (model.Sin - s) / q
            s, x = eval_func(x=xlim, func=dx_xs, n=2)
            lines += ax.plot(s, x, color=c)
    
    if 2 in var or model[2] in var or "q" in var:
        s, x = eval_func(x=xlim, func=dq_xs, n=n_pts)
        label = _tex_dot(model[2]) if model.quota == "cell" else _tex_dot("q")
        lines += ax.plot(s, x, label=label, linestyle="--")

    return lines
