from abc import ABC, abstractmethod
from numbers import Number
import numpy as np

# define abstract classes

class GrowthRate(ABC):
    @abstractmethod
    def __init__(self, *args):
        for arg in args:
            if isinstance(arg, Number) and arg <= 0:
                raise ValueError("GrowthRate parameters must be positive numbers")
            if isinstance(arg, np.ndarray):
                # check they have same dims and are all positive
                pass

    @abstractmethod
    def __call__(self, *args): pass

    def __repr__(self) -> str:
        s = f"{type(self).__name__}"
        d = vars(self).copy()
        if d:
            s += "(" + ", ".join([f"{i[0]}={i[1]}" for i in d.items()]) + ")"
        return s

class GrowthRateSimple(GrowthRate):
    @abstractmethod
    def derivative(self, arg): pass

class GrowthRateSX(GrowthRate):
    @abstractmethod
    def __call__(self, s, x): pass

class GrowthRateComposite(GrowthRate):
    @abstractmethod
    def __init__(self, *args): pass

# classical growth functions

class Monod(GrowthRateSimple):
    def __init__(self, max_growth, michaelis_menten):
        GrowthRate.__init__(self, max_growth, michaelis_menten)
        self.max = max_growth
        self.k = michaelis_menten
    def __call__(self, s):
        func = lambda s: self.max * s / (self.k + s)
        if isinstance(s, Number):
            return func(s) if s > 0 else 0
        if isinstance(s, np.ndarray):
            r = np.zeros(s.shape)
            ind = s >= 0
            r[ind] = func(s[ind])
            return r
        if isinstance(s, list):
            return self.__call__(np.array(s))
        raise ValueError("'s' has to be a number or a numeric array")
    def inverse(self, r):
        func = lambda r: self.k * r / (self.max - r)
        if isinstance(r, Number):
            if r <= 0:
                return 0
            if r >= self.max:
                return np.nan
            return func(r)
        if isinstance(r, np.ndarray):
            s = np.zeros(r.shape)
            ind = r > 0
            s[ind] = func(r[ind])
            ind = r >= self.max
            s[ind] = np.nan
            return s
        if isinstance(r, list):
            return self.__call__(np.array(r))
        raise ValueError("'r' has to be a number or a numeric array")
    def derivative(self, s):
        func = lambda s: self.max * self.k / (self.k + s)**2
        if isinstance(s, Number):
            return func(s) if s > 0 else 0
        if isinstance(s, np.ndarray):
            r = np.zeros(s.shape)
            ind = s > 0
            r[ind] = func(s[ind])
            return r
        if isinstance(s, list):
            return self.__call__(np.array(s))
        raise ValueError("'s' has to be a number or a numeric array")

class ShiftedMonod(Monod):
    def __init__(self, max_growth, michaelis_menten, min_substrate):
        GrowthRate.__init__(self, max_growth, michaelis_menten, min_substrate)
        self.max = max_growth
        self.k = michaelis_menten
        self.shift = min_substrate
    def __call__(self, s):
        return super().__call__(s - self.shift)
    def inverse(self, r):
        return self.shift + super().inverse(r)
    def derivative(self, s):
        return super().derivative(s - self.shift)

class Haldane(GrowthRateSimple):
    def __init__(self, max_growth, michaelis_menten, inhibition):
        GrowthRate.__init__(self, max_growth, michaelis_menten, inhibition)
        self.km = michaelis_menten
        self.ki = inhibition
        self.mu0 = max_growth * (1 + np.sqrt(self.km / self.ki))
        self.argmax = np.sqrt(self.km * self.ki)
        self.max = self.__call__(self.argmax)
    def __call__(self, s):
        func = lambda s: self.mu0 * s / (self.km + s + s**2/self.ki)
        if isinstance(s, Number):
            return func(s) if s > 0 else 0
        if isinstance(s, np.ndarray):
            r = np.zeros(s.shape)
            ind = s > 0
            r[ind] = func(s[ind])
            return r
        if isinstance(s, list):
            return self.__call__(np.array(s))
        raise ValueError("'s' has to be a number or a numeric array")
    def preimages(self, r):
        def func(r):
            _b = self.mu0/r - 1
            sqrt_delta = np.sqrt(_b**2 - 4*self.km/self.ki)
            ki2 = self.ki / 2
            return ki2 * (_b - sqrt_delta), ki2 * (_b + sqrt_delta)
        if isinstance(r, Number):
            if r <= 0:
                return 0, 0
            if r > self.max:
                return np.nan, np.nan
            if r == self.max:
                return self.argmax, self.argmax
            return func(r)
        if isinstance(r, np.ndarray):
            r = r.flatten()
            s = np.zeros((len(r), 2))
            ind = r > 0
            s[ind,0], s[ind,1] = func(r[ind])
            ind = r > self.max
            s[ind,:] = np.nan
            return s
        if isinstance(r, list):
            return self.__call__(np.array(r))
        raise ValueError("'r' has to be a number or a numeric array")
    def derivative(self, s):
        def func(s):
            s2ki = s**2 / self.ki
            return self.mu0 * (self.km - s2ki) * np.power(self.km + s + s2ki, -2)
        if isinstance(s, Number):
            return func(s) if s > 0 else 0
        if isinstance(s, np.ndarray):
            r = np.zeros(s.shape)
            ind = s > 0
            r[ind] = func(s[ind])
            return r
        if isinstance(s, list):
            return self.__call__(np.array(s))
        raise ValueError("'s' has to be a number or a numeric array")

class Contois(GrowthRateSX):
    def __init__(self, max_growth, contois):
        GrowthRate.__init__(self, max_growth, contois)
        self.max = max_growth
        self.k = contois
    def __call__(self, s, x):
        func = lambda s, x: self.max * s / (self.k*x + s)
        if isinstance(s, Number) and isinstance(x, Number):
            return func(s, x) if s > 0 and x > 0 else 0
        if isinstance(s, np.ndarray) or isinstance(x, np.ndarray):
            s, x = np.ndarray(s).flatten(), np.ndarray(x).flatten()
            r = np.zeros((len(s), len(x)))
            s_ind, x_ind = s > 0, x > 0
            r[s_ind, x_ind] = func(s[s_ind], x[x_ind])
            return r
        if isinstance(s, list) or isinstance(x, list):
            return self.__call__(np.array(s), np.array(x))
        raise ValueError("arguments have to be numbers or numeric arrays")

# composite growth functions

class VarYieldMonod(GrowthRateComposite):
    def __init__(self, rho: Monod, mu: ShiftedMonod):
        self.rho = rho
        self.mu = mu
    def __call__(self, s):
        return self.mu(self.__q(s))
    def __q(self, s):
        a = self.mu.shift + self.rho(s) / self.mu.max
        b = (4*(self.mu.shift - self.mu.k) / self.mu.max) * self.rho(s)
        return (a + np.sqrt(a**2 - b)) / 2
    def inverse(self, r):
        return self.rho.inverse(r * self.mu.inverse(r))

class ABGrowthRate(GrowthRateComposite):
    def __init__(self, mu_B: Monod, phi_A: VarYieldMonod, growth_yield, production_yield):
        GrowthRate.__init__(self, growth_yield, production_yield)
        self.mu_B = mu_B
        self.phi_A = phi_A
        self.Y = growth_yield
        self.beta = production_yield
    def __call__(self, s):
        raise NotImplementedError("cannot be calculated")
    def inverse(self, r):
        return self.mu_B.inverse(r) + self.phi_A.inverse(r) / (self.Y * self.beta)